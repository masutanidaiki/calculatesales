package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String SALES_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NAME_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String BRANCH_FILE_INVALID_CODE = "の支店コードが不正です";
	private static final String COMMODITY_FILE_INVALID_CODE = "の商品コードが不正です";
	private static final String DIGIT_OVER = "合計金額が10桁を超えました";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		if(args.length !=1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}", "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]+$", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i=0; i< files.length; i++) {
			if(files[i].getName().matches("^[0-9]{8}.+rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);

		for(int i=0; i<rcdFiles.size()-1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former)!=1) {
				System.out.println(FILE_NAME_NOT_SERIAL_NUMBER);
				return;
			}
		}

		BufferedReader br = null;

		for(int i=0; i< rcdFiles.size(); i++) {
			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 一行ずつ読み込む
				List<String> fileContents = new ArrayList<>();

				String line;
				while((line = br.readLine()) != null) {
					fileContents.add(line);
				}

				String fileName = rcdFiles.get(i).getName();

				if(fileContents.size() !=3) {
					System.out.println(fileName + SALES_FILE_INVALID_FORMAT);
					return;
				}

				//支店コード
				String branchCode = fileContents.get(0);
				//売上金額
				String salesMoney = fileContents.get(2);
				//商品コード
				String commodityCode = fileContents.get(1);

				if(! branchSales.containsKey(branchCode)) {
					System.out.println(fileName + BRANCH_FILE_INVALID_CODE);
					return;
				}

				if(! commoditySales.containsKey(commodityCode)) {
					System.out.println(fileName + COMMODITY_FILE_INVALID_CODE);
					return;
				}

				if(! salesMoney.matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(salesMoney);

				Long saleAmount = branchSales.get(branchCode) + fileSale;
				Long commoditySaleAmount = commoditySales.get(commodityCode) + fileSale;

				if(saleAmount >= 10000000000L) {
					System.out.println(DIGIT_OVER);
					return;
				}
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println(DIGIT_OVER);
					return;
				}

				branchSales.put(branchCode, saleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap, String match, String type) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if((items.length !=2) || (!items[0].matches(match))) {
					System.out.println(type + FILE_INVALID_FORMAT);
					return false;
				}

				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);

			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw =  new BufferedWriter(fw);

			for(String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();

			}
		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
